const schedule = require('node-schedule-tz');

let timer = {};


let time = (name, start, seconds) => {
    let rule = new schedule.RecurrenceRule();
    rule.start = start;
    rule.second = seconds;
    // rule.hour = hours;

    timer[name] = schedule.scheduleJob({start: start, rule: `*/9 * * * * *`}, () => {
        console.log(name);
      });
}
let startTime = Date.now() + 5000
time('timerOne', startTime, 9)

setTimeout(()=> {
    timer['timerOne'].cancel();
},15000)
  
