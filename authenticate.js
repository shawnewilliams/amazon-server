const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const User = require('./models/users');
const JwtStrategy = require('passport-jwt').Strategy;
const ExtractJwt = require('passport-jwt').ExtractJwt;
const jwt = require('jsonwebtoken');
const config = require('./config/config');

exports.local = passport.use(User.createStrategy());
passport.serializeUser(User.serializeUser()); 
passport.deserializeUser(User.deserializeUser());

exports.getToken = (user) => {
    return jwt.sign(user, config.jwt_secretKey,
    {expiresIn: 3600});
};

const opts = {};
opts.jwtFromRequest = ExtractJwt.fromAuthHeaderAsBearerToken();
opts.secretOrKey = config.jwt_secretKey;
opts.passReqToCallback = true;

exports.jwtPassport = passport.use(new JwtStrategy(opts, (req, jwt_payload, done) => {
    console.log("JWT payload: ", jwt_payload._id);
    User.findOne({_id: jwt_payload._id}, (err, user) => {
        if (err) {
            return done(err, false);
        }
        if (user) {
            return done(null, user);
        } else {
            return done(null, false);
            // or you could create a new account
        }
    });
}));

exports.verifyUser = passport.authenticate('jwt', {session: false});

// module.exports.local = local;
// module.exports.getToken = getToken;
// module.exports.jwtPassport = jwtPassport;
// module.exports.verifyUser = verifyUser;