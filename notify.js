const config = require('./config/config');
const User = require('./models/users');
const TrackedAsin = require('./models/tracked-asin');

const SetInterval = require('set-interval');
const amazonMws = require('amazon-mws')(config.accessKey, config.AWS_SECRET_ACCESS_KEY);


const api = require('./api-calls');
const schedule = require('node-schedule-tz');

let timer = {};

let NotifmeSdk = require('./node_modules/notifme-sdk').default
let notifmeSdk = new NotifmeSdk({
    channels: {
        email: {
          providers: [
        //       {
        //     type: 'sendgrid',
        //     apiKey: config.SENDGRID_API_KEY
        //   }
        {
            type: 'smtp',
            host: 'smtp.gmail.com',
            port: 465,
            secure: true,
            auth: {
              user: config.EMAIL,
              pass: config.AMZ_APP_EMAIL_PASS
            }
        }]
        },
        sms: {
            providers: [{
              type: 'clickatell',
              apiKey: config.CLICKATELL_API_KEY // One-way integration API key
            }]
        }
    }
})

let getUser = async (user_id) => {
    try {
        let user = await User.findById(user_id);
        if(user) {
            let credentials = {
                email: user.email,
                phoneNumber: user.phoneNumber,
                firstName: user.firstName,
            }
            return credentials;
        } else {
            return ("No User Found")
        }
    } catch (e) {
        throw new Error(e);
    }
};

let getTracked = async (docId) => {
    try {
        let tracked = await TrackedAsin.findById(docId);
        if(tracked) {
            return tracked;   
        } else {
            return ("No ASIN Found")
        }
    } catch (e) {
        throw new Error(e);
    }
};

let notifiedTimer = async (asin, minPrice, user_id, docId) => {
    try {
        let response = await api.getAsinFromAmazon(asin, user_id);
        let prices = api.getPrices(response, minPrice); 
        // let response = [];
        // let prices = [];
        
        // console.log('prices from notify', prices)
        // console.log("nofify user", user)
        if (!prices.length) {

            SetInterval.start(async() => {
                if (prices.length > 0) {
                    SetInterval.clear(docId);

                    notify(asin, minPrice, user_id, docId);
                    console.log('send notification');
                    
                    let myDate = new Date(Date.now());
                    // myDate.setFullYear(2013);
                    // myDate.setMonth(02);
                    // myDate.setDate(28);
                    // myDate.setHours(12);
                    // myDate.setMinutes(00);
                    // myDate.setSeconds(00); 
                    // console.log(myDate.toString())
                    let startTimer = myDate.getTime(); 
                    timer[docId] = schedule.scheduleJob('08 16 * * *', async () => {
                        // let doc = await TrackedAsin.findById(docId);
                        console.log("notify() inside timer",docId);
                        await notify(asin, minPrice, user_id, docId);
                        });
                } else {
                    console.log('inside SetInterval')
                    response = await api.getAsinFromAmazon(asin, user_id);
                    prices = api.getPrices(response, minPrice);
                }
            }, 1620000, docId);

        } else {
            let tracked = await getTracked(docId);
            if(!tracked.notified){
                notify(asin, minPrice, user_id, docId);
            }
            // var myDate = new Date(Date.now());
            // myDate.setFullYear(2013);
            // myDate.setMonth(02);
            // myDate.setDate(28);
            // myDate.setHours(12);
            // myDate.setMinutes(00);
            // myDate.setSeconds(00); 
            // console.log(myDate.toString())
            // let startTimer = myDate.getTime(); 
            timer[docId] = schedule.scheduleJob('08 16 * * *', async () => {
                // let doc = await TrackedAsin.findById(docId);
                console.log("notify() inside timer",docId);
                await notify(asin, minPrice, user_id, docId);
            });
            
        }
    } catch (e) {
        throw new Error(e);
    }
}

let stopNotifiedTimer = (docId) => {
    console.log('Canceling notifiedTimer')
    SetInterval.clear(docId);
    if(timer[docId]){
        timer[docId].cancel();
        delete timer[docId];
    }  
}

let notify = async (asin, minPrice, user_id, docId) => {
    try {
        let item = await TrackedAsin.findById(docId);
        console.log("***product****", item.title)
        
        let response = await api.getAsinFromAmazon(asin, user_id);
        let prices = api.getPrices(response, minPrice);   
        let user = await getUser(user_id);
        if(prices.length > 0) {
            let displayPrices = prices.join(', $')
            let notificationRequest = {
                email: {
                    from: 'amz.price.info@gmail.com',
                    to: `${user.email}`,
                    subject: `Tracked Product: ${item.title}`,
                    html: `<p>Hello ${user.firstName},</p>
                    <p>Your tracked product: <br> 
                    <strong>Asin: ${asin}<br>
                    ${item.title}</strong><br><br>
                    is being sold for less than the minimum price of $${minPrice}</p>
                    <p>The prices are: $${displayPrices}</p>
                    <a href="https://www.amazon.com/gp/product/${asin}/?tag=108f29-20">https://www.amazon.com/gp/product/${asin}/?tag=108f29-20</a>
                    `,
                    replyTo: config.EMAIL,
                    text: `Hello ${user.firstName}! How are you?`,
                },
                // sms: {
                //     to: `1${user.phoneNumber}`,
                //     text: `Hello ${user.firstName}! How are you? Your ASIN: ${asin} is being sold for less than the minimum price of $${minPrice}
                //     https://www.amazon.com/gp/product/${asin}/?tag=108f29-20`
                // }
            }
            let tracked = await TrackedAsin.findByIdAndUpdate(docId,{ $set: {notified: true}}); 
            // console.log(tracked)
            notifmeSdk.send(notificationRequest).then(console.log).catch(e => e);
        }
    } catch (e) {
        throw new Error(e);
    }
};

let restartTracking = async () => {
    try {
        let trackedList = await TrackedAsin.find();
        console.log("restartTracking: ",trackedList)
        trackedList.map((item) => {
            notifiedTimer(item.asin, item.minPrice, item._creator, item._id)
        });
    } catch (e) {
        throw new Error(e)
    }
}

    
module.exports.notify = notify;
module.exports.notifiedTimer = notifiedTimer;
module.exports.stopNotifiedTimer = stopNotifiedTimer;
module.exports.restartTracking = restartTracking;
