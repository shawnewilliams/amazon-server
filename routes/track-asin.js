var express = require('express');
var router = express.Router();
const api = require('../api-calls');
const notify = require('../notify');
const SetInterval = require('set-interval');
const authenticate = require('../authenticate');
const cors = require('./cors');

router.get('/', function(req, res, next) {
    res.send({ title: 'Express' });
});
router.route('/')
.options(cors.corsWithOptions, (req, res, next) => {res.sendStatus(200)})  
.post(cors.corsWithOptions, authenticate.verifyUser, async (req, res, next) => {
    // console.log("*****req.user._id**** track-asin", req.user._id)
    let asin = req.body.asin;
    let minPrice = req.body.minPrice;
    let time = req.body.time;
    let run = req.body.run;
    let name = req.body.name;
    try {
        let response = await api.getAsinFromAmazon(asin, req.user._id);
        // console.log('response', response);
        let prices = api.getPrices(response, minPrice);
        console.log('prices from track-asin', prices);
        if (run === 'go') {
            notify.notify(asin, minPrice, req.user._id);           
            SetInterval.start(() => {
                console.log(name);
                notify.notify(asin, minPrice, req.user._id);
            }, time, name)
                
        } else if (run === 'stop') {
            SetInterval.clear(name);
        };
        // res.statusCode = 200;
        // res.setHeader("Access-Control-Allow-Origin", "http://localhost:3001");
        res.send(prices);
    } catch (e) {
        // res.statusCode = 404;
        // res.setHeader("Access-Control-Allow-Origin", "http://localhost:3001");
        next(e);
    };
});

module.exports = router;
