const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const TrackedAsin = require('../models/tracked-asin');
const cors = require('./cors');
const authenticate = require('../authenticate');
const SetInterval = require('set-interval');
const api = require('../api-calls');
const notify = require('../notify');

const router = express.Router();

router.use(bodyParser.json());

router.route('/')
.options(cors.corsWithOptions, (req, res, next) => {res.sendStatus(200)})
.get(cors.cors, authenticate.verifyUser, async (req,res) => {
    try {
        const asins = await TrackedAsin.find({_creator: req.user._id})
        res.send(asins)
    } catch (e) {
        next(e);
    }
})
.post(cors.corsWithOptions, authenticate.verifyUser, async (req, res, next) => {
    try {
        console.log('request from react', req.body);
        console.log("*****req.user._id****", req.user._id)
        
        const tracked = new TrackedAsin({
            asin: req.body.asin,
            image: req.body.image,
            price: req.body.price,
            title: req.body.title,
            minPrice: req.body.minPrice,
            // hours: req.body.hours,
            _creator: req.user._id
        });
        console.log(tracked);
        const doc = await tracked.save();

// *************** for tracking with one route. Add clear to delete route
        let response = await api.getAsinFromAmazon(req.body.asin, req.user._id);
        let prices = api.getPrices(response, req.body.minPrice);
        // console.log('prices from track-asin', prices);
        
        // notify.notify(asin, minPrice, req.user._id);           
        // SetInterval.start(() => {
            // console.log(doc._id);
            // notify.notify(req.body.asin, req.body.minPrice, req.user._id);
            notify.notifiedTimer(req.body.asin, req.body.minPrice, req.user._id, doc._id)
        // }, req.body.hours * 3600000, doc._id)
        // }, 2000, doc._id)
                
        res.send(prices);
// ********************

        
        // res.statusCode = 200;
        // res.setHeader("Access-Control-Allow-Origin", "*");
        // res.send(track);
    } catch (e) {
        // res.setHeader("Access-Control-Allow-Origin", "http://localhost:3001", "http://localhost:3000");
        res.status(400)
        next(e);
    }
});

router.route('/')
.options(cors.corsWithOptions, (req, res, next) => {res.sendStatus(200)})
.delete(cors.corsWithOptions, authenticate.verifyUser, async (req,res, next) => {
    try {
        console.log("*****req.user._id****", req.user._id)
        const deleted = await TrackedAsin.findOneAndRemove({_id: req.body.id, _creator: req.user._id});
        // SetInterval.clear(deleted._id);
        notify.stopNotifiedTimer(deleted._id);
        res.send(deleted);
    } catch (e) {
        next(e);
    }
})

module.exports = router;