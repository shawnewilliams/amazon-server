var express = require('express');
var router = express.Router();
const path = require('path');

/* GET home page. */
// router.get('/', function(req, res, next) {
//   res.send({ title: 'Express' });
// });

router.get('*', (req,res) => {
  res.sendFile(path.join(__dirname, '../public/build/index.html'));
 });

module.exports = router;
