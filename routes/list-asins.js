var express = require('express');
var router = express.Router();
const api = require('../api-calls');
const authenticate = require('../authenticate');
const cors = require('./cors');

  
router.route('/')
.options(cors.corsWithOptions, (req, res, next) => {res.sendStatus(200)})
.get(cors.corsWithOptions, authenticate.verifyUser, async (req, res, next) => {
    try {
        console.log("*****req.user._id**** list-asins", req.user._id)
        const asins = await api.getMyAsins(req.user._id);
        console.log('getAsinsList()', asins);
        res.statusCode = 200;
        res.setHeader("Access-Control-Allow-Origin", "http://localhost:3001");
        res.send(asins)
    } catch (e) {
        res.setHeader("Access-Control-Allow-Origin", "http://localhost:3001");
        next(e);
    }
});

module.exports = router;
