const express = require('express');
const bodyParser = require('body-parser');
const jwt = require('jsonwebtoken');
const router = express.Router();
const passport = require('passport');
const User = require('../models/users');
const authenticate = require('../authenticate');
const cors = require('./cors');
const config = require('../config/config')

const jwt_secretKey = config.jwt_secretKey;

/* GET users listing. */
router.options('*', cors.corsWithOptions, (req, res) => {res.sendStatus(200); });
router.get('/', cors.corsWithOptions, (req, res, next) => {
  User.find({})
    .then((user) => {
        res.statusCode = 200;
            res.setHeader('Content-Type', 'application/json');
            res.json(user);
        }, (err) => next(err))
        .catch((err) => next(err));
// })
  
  // res.send('respond with a resource');
});

router.route('/signup')
.options(cors.corsWithOptions, (req, res, next) => {res.sendStatus(200)})
.post(cors.corsWithOptions, async (req, res, next) => {
  console.log('signing up user');
  try {
    user = new User({
      username: req.body.username,
      firstName: req.body.firstName,
      lastName: req.body.lastName,
      email: req.body.email,
      phoneNumber: req.body.phoneNumber
    })
    await user.setPassword(req.body.password);
    await user.save();
    await User.authenticate()(req.body.username, req.body.password);
    res.statusCode = 200;
    res.setHeader('Content-Type', 'application/json'); 
    res.send({success: true, status: 'Registration Successful!'});
  } catch (err) {
    console.log(err);
    res.statusCode = 500; 
    res.setHeader('Content-Type', 'application/json'); 
    res.json({err: err});
    next(err)
  }
});

router.post('/login', cors.corsWithOptions, (req, res, next) => {
  passport.authenticate('local', async (err, user, info) => {
    if(err) {
      console.log('err 1')
      return next(err);
    }
    if (!user) {
      console.log('no user')
      res.setHeader('Content-Type', 'application/json')
      return res.send({success: false, status: 'Login Unsuccessful!', err: info}).status(401);
    }
    req.logIn(user, (err) => {
      if(err) {
        console.log('err 2')
        res.setHeader('Content-Type', 'application/json')
        return res.send({success: false, status: 'Login Unsuccessful!', err: 'Could not log in user'}).status(401);
      }
      const token = authenticate.getToken({_id: req.user._id});
      console.log('we got here')
      return res.send({success: true, status: 'Login Successful!', token: token});
    });
  }) (req, res, next);
});

router.route('/credentials')
.options(cors.corsWithOptions, (req, res, next) => {res.sendStatus(200)})
.put(cors.cors, authenticate.verifyUser, async (req, res, next) => {
  console.log(req.user._id)
  try {
    const data = {
      sellerID: req.body.sellerID,
      accessKey: req.body.accessKey,
      authToken: req.body.authToken,
      marketplaceID: req.body.marketplaceID,
      AWS_SECRET_ACCESS_KEY: req.body.AWS_SECRET_ACCESS_KEY
    };
    const credentials = jwt.sign(data, jwt_secretKey);

    const updated = await User.findByIdAndUpdate(req.user._id, {
        firstname: req.body.firstname,
        lastname: req.body.lastname,
        email: req.body.email,
        phoneNumber: req.body.phoneNumber,
        credentials: credentials
    })
    res.statusCode = 200;
      res.setHeader('Content-Type', 'application/json')
      res.send(jwt.verify(updated.credentials, jwt_secretKey));
  } catch (e) {
    next(e);
  } 
})

.delete(cors.corsWithOptions, async (req,res, next) => {
    try {
        const deleted = await TrackedAsin.findByIdAndRemove(req.params._id);
        res.send(deleted);
    } catch (e) {
        next(e);
    }
});

router.route('/logout')
.options(cors.corsWithOptions, (req, res, next) => {res.sendStatus(200)})
.get(cors.corsWithOptions, authenticate.verifyUser, (req, res, next) => {
  if (req.user) {
    req.logout();
    res.clearCookie('session-id');
    res.send({loggedOut: true});
  } else {
    var err = new Error('You are not logged in!');
    err.status = 403;
    next(err);
  }
});



module.exports = router;
