var express = require('express');
var router = express.Router();
const api = require('../api-calls');
const authenticate = require('../authenticate');
const cors = require('./cors');
  
router.route('/:query')
.options(cors.corsWithOptions, (req, res, next) => {res.sendStatus(200)})
.get(cors.corsWithOptions, async (req, res, next) => {
    try {
        let userId = null;
        !req.user ? userId = null : userId = req.user._id
        console.log("*****req.user._id**** search/", userId)
        let products = await api.listMatchingProducts(req.params.query, userId);
        // res.setHeader("Access-Control-Allow-Origin", "http://localhost:3001");
        res.send(products);
    } catch (e) {
        // res.setHeader("Access-Control-Allow-Origin", "http://localhost:3001");
        // res.status(404);
        // console.log('Search Error', e);
        next(e);
    }  
});

router.route('/asin/:asin')
.options(cors.corsWithOptions, (req, res, next) => {res.sendStatus(200)})
.get(cors.corsWithOptions, authenticate.verifyUser, async (req, res, next) => {
    let asin = req.params.asin;
    try {
        let product = await api.getAsinDetails(asin, req.user._id);
        res.statusCode = 200;
        // res.setHeader("Access-Control-Allow-Origin", "http://localhost:3001");
        res.send(product);
    } catch (e) {
        console.log('error products', e);
        // res.setHeader("Access-Control-Allow-Origin", "http://localhost:3001");
        next(e); 
    }
})

module.exports = router;
