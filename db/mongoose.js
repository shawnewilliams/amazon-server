var mongoose = require('mongoose');

mongoose.Promise = global.Promise;
mongoose.connect(process.env.MONGODB_URI).then((db) => {
    console.log('Mongoose Connected')
}).catch((e) => console.log(e));

module.exports = { mongoose };