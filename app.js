// var env = process.env.NODE_ENV || 'development';
// console.log('env *****', env);

// if( env === 'development' || env === 'test') {
//      var dbConfig = require('./config/db-config.json');
//      console.log(config);
//      var envConfig = config[env];
//      Object.keys(envConfig).forEach((key) => {
//         process.env[key] = envConfig[key];
//      });
// }
require('./config/db-config');
const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');

const bodyParser = require('body-parser');
const { ObjectID } = require('mongodb');
const passport = require('passport');

const indexRouter = require('./routes/index');
const usersRouter = require('./routes/users');
const trackAsinRouter = require('./routes/track-asin');
const listAsinsRouter = require('./routes/list-asins');
const searchRouter = require('./routes/search');
const trackedAsinRouter = require('./routes/tracked-asin');

const {mongoose} = require('./db/mongoose');

const app = express();

// mongoose.Promise = global.Promise;
// mongoose.connect(process.env.MONGODB_URI).then((db) => {
//     console.log('Mongoose Connected')
// }).catch((e) => console.log(e));

app.use(passport.initialize());

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public/build')));



// app.use('/', indexRouter);
app.use('/users', usersRouter);
app.use('/track', trackAsinRouter);
app.use('/list-asins', listAsinsRouter);
app.use('/search', searchRouter);
app.use('/tracked-asin', trackedAsinRouter);

// Use React Build
app.get('*', (req,res) => {
    res.sendFile(path.join(__dirname, 'public/build/index.html'));
});

module.exports = app;
