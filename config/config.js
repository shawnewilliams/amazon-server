// require('dotenv').config()

const sellerID = process.env.sellerID;
const accessKey = process.env.accessKey;
const authToken = process.env.authToken;
const marketplaceID = process.env.marketplaceID;
const AWS_SECRET_ACCESS_KEY = process.env.AWS_SECRET_ACCESS_KEY;
const jwt_secretKey = process.env.jwt_secretKey;
const baseURL = "http://localhost:3000";
const AMZ_APP_EMAIL_PASS = process.env.AMZ_APP_EMAIL_PASS;
const CLICKATELL_API_KEY = process.env.CLICKATELL_API_KEY;
const SENDGRID_API_KEY = process.env.SENDGRID_API_KEY;
const EMAIL = process.env.EMAIL;

module.exports = {
    sellerID,
    accessKey,
    authToken,
    marketplaceID,
    AWS_SECRET_ACCESS_KEY,
    jwt_secretKey,
    AMZ_APP_EMAIL_PASS,
    CLICKATELL_API_KEY,
    SENDGRID_API_KEY,
    baseURL,
    EMAIL
}



