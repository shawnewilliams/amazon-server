const User = require('./models/users');
const jwt = require('jsonwebtoken');
const config = require('./config/config');
// const {OperationHelper} = require('apac');

// ************ AWS marketing product api
// const opHelper = new OperationHelper({
//     awsId:     '*******',
//     awsSecret: '*******',
//     assocId:   '*******'
// });

// ************ AWS marketing product api ***********
// opHelper.execute('ItemSearch', {
//     'SearchIndex': 'Books',
//     'Keywords': 'harry potter',
//     'ResponseGroup': 'ItemAttributes,Offers'
//   }).then((response) => {
//       console.log("Results object: ", response.result);
//       console.log("Raw response body: ", response.responseBody);
//   }).catch((err) => {
//       console.error("Something went wrong! ", err);
//   });
// ************ AWS marketing product api ***********


// *********** User User Credentials ***************
// const getCredentials = async (userID) => {
//     try {
//         const user = await User.findById(userID);
//             if (user) {
//                 let credentials = jwt.verify(user.credentials, jwt_secretKey);
//                 return credentials;
//             }
//     } catch (e) {
//         throw new Error('Unable to get user credentials')
//     }
// }
// *********** User User Credentials ***************

const getCredentials = async (userID) => {
    try {
        let credentials = {
            sellerID: config.sellerID,
            authToken: config.authToken,
            marketplaceID: config.marketplaceID
        };
        if (!userID) {
            return credentials; 
        } else {
            const user = await User.findById(userID);
                if (user.credentials) {
                    console.log('api-calls user.credentials', user.credentials)
                    let userCredentials = jwt.verify(user.credentials, config.jwt_secretKey);
                    if(!userCredentials.sellerID || !userCredentials.authToken || !userCredentials.marketplaceID ) {
                        return credentials;
                    } else {
                        return userCredentials;
                    }
                } else {
                    return credentials;
                }
            }
            
    } catch (e) {
        console.log("getCredentials() ERROR:", e);
        throw new Error("Unable to get credentials", e)
    }
}

var amazonMws = require('amazon-mws')(config.accessKey, config.AWS_SECRET_ACCESS_KEY);

const getFees = async (asin, listingPrice, shippingPrice) => {
    try {
        // let credentials = {
        // sellerID: config.sellerID,
        // accessKey: config.accessKey,
        // authToken: config.authToken,
        // marketplaceID: config.marketplaceID,
        // AWS_SECRET_ACCESS_KEY: config.AWS_SECRET_ACCESS_KEY
        // }
        let credentials = await getCredentials(user_id);
        let data = await amazonMws.products.search({
            'FeesEstimateRequestList.FeesEstimateRequest.1.MarketplaceId': credentials.marketplaceID,
            'FeesEstimateRequestList.FeesEstimateRequest.1.IdType': 'ASIN',
            'FeesEstimateRequestList.FeesEstimateRequest.1.IdValue': asin,
            'FeesEstimateRequestList.FeesEstimateRequest.1.IsAmazonFulfilled': true,
            'FeesEstimateRequestList.FeesEstimateRequest.1.Identifier': 'request1',
            'FeesEstimateRequestList.FeesEstimateRequest.1.PriceToEstimateFees.ListingPrice.CurrencyCode': 'USD',
            'FeesEstimateRequestList.FeesEstimateRequest.1.PriceToEstimateFees.ListingPrice.Amount': listingPrice,
            'FeesEstimateRequestList.FeesEstimateRequest.1.PriceToEstimateFees.Shipping.CurrencyCode': 'USD',
            'FeesEstimateRequestList.FeesEstimateRequest.1.PriceToEstimateFees.Shipping.Amount': shippingPrice,
            'Version': '2011-10-01',
            'Action': 'GetMyFeesEstimate',
            'SellerId': credentials.sellerID,
            'MWSAuthToken': credentials.authToken,
            'MarketplaceId': credentials.marketplaceID,
            'ItemCondition': 'New',
            'ASINList.ASIN.1': asin
        });
        return data;
    } catch (e) {
        console.log("getFees() ERROR:", e);
        throw new Error("Unable to get fees", e);
    };
};

// getFees('B00VP65JWM',23.08, 0).then((result) => {
//     console.log(JSON.stringify(result, null, 4));
// })

const getAsinFromAmazon = async (asin, user_id) => {
    try {
        let credentials = await getCredentials(user_id);
        let data = await amazonMws.products.search({  
            'Version': '2011-10-01',
            'Action': 'GetLowestOfferListingsForASIN',
            'SellerId': credentials.sellerID,
            'MWSAuthToken': credentials.authToken,
            'MarketplaceId': credentials.marketplaceID,
            'ItemCondition': 'New',
            'ASINList.ASIN.1': asin
        });
        if (data.status === 'Success') {
            return data;
        } else {
            throw new Error('getAsinFromAmazon() - Invalid Asin')
        };
    } catch (e) {
        console.log("getAsinFromAmazon() ERROR:", e);
        throw new Error(e);
    };
};

const getCompetitivePricingForASIN = async (asin, user_id) => {
    try {
        let credentials = await getCredentials(user_id);
        let data = await amazonMws.products.search({  
            'Version': '2011-10-01',
            'Action': 'GetCompetitivePricingForASIN',
            'SellerId': credentials.sellerID,
            'MWSAuthToken': credentials.authToken,
            'MarketplaceId': credentials.marketplaceID,
            'ASINList.ASIN.1': asin
        });
        if (data.status === 'Success') {
            return data;
        } else {
            throw new Error('getAsinFromAmazon() - Invalid Asin')
        };
    } catch (e) {
        console.log("getCompetitivePricingForASIN() ERROR:", e);
        throw new Error("Unable to getCompetitivePricingForASIN",e);
    };
};

getLowestPrice = async (asin, user_id) => {
    try {
        let data = await getAsinFromAmazon(asin, user_id);
        let pricesArray = data.Product.LowestOfferListings.LowestOfferListing;
        let prices = [];
        let price = null;
        // console.log(pricesArray);
        if (!pricesArray) {
            price = "Unavailable";
        } else if (!pricesArray[0]) {
            price = pricesArray.Price.ListingPrice.Amount
        } else if (pricesArray[0]) {
            prices = pricesArray.map((product) => {
                return product.Price.ListingPrice.Amount;  
            });
            price = Math.min.apply(null, prices);
        }
        return price
    } catch (e) {
        console.log("getLowestPrice() ERROR:", e);
        throw new Error("Unabel to get lowest price", e);
    }
}

getPrices = (item, minPrice) => {
    // console.log('(getPrices)', item)
     if (item.status === 'Success') {
        let data = item.Product.LowestOfferListings.LowestOfferListing;
        let prices = [];
        if (!data) {
            prices = []
        } else if (data[0]) {
            prices = data.map((product) => {
                if (+product.Price.ListingPrice.Amount < +minPrice) {
                    return product.Price.ListingPrice.Amount;
                }
            });
            prices = prices.filter((price) => {
                return price;
            })
        } else {
            +data.Price.ListingPrice.Amount < +minPrice ? prices = [data.Price.ListingPrice.Amount] : prices = [];
        }
        // console.log('getPrices() api-calls', prices)
        return prices;
     } else {
         console.log("getPrices() ERROR:", e);
         throw new Error("Unable to get prices", e);
     }
};

listMatchingProducts = async (query, user_id) => {
    try {
        let credentials = await getCredentials(user_id);
        let product = await amazonMws.products.search({
            'Version': '2011-10-01',
            'Action': 'ListMatchingProducts',
            'SellerId': credentials.sellerID,
            'MWSAuthToken': credentials.authToken,
            'MarketplaceId': credentials.marketplaceID,
            'ItemCondition': 'New',
            'Query': query
        });
        // console.log(JSON.stringify(product.Products, null, 2))
        let productInfo = {};
        let productArray = []
        let price = null;
        if(!product.Products.Product) {
            return ['Nothing Found'];
        }
        
        if(!product.Products.Product.length) {

            // console.log(product.Products.Product.Identifiers.MarketplaceASIN.ASIN);
            // price = await getLowestPrice(product.Products.Product.Identifiers.MarketplaceASIN.ASIN, user_id);
            // productInfo.price = price;
            productInfo.asin = product.Products.Product.Identifiers.MarketplaceASIN.ASIN; 
            productInfo.image = product.Products.Product.AttributeSets.ItemAttributes.SmallImage.URL
            productInfo.title = product.Products.Product.AttributeSets.ItemAttributes.Title;
           
            productArray.push(productInfo);

        } else {
            for (let item of product.Products.Product) {
                // price = await getLowestPrice(item.Identifiers.MarketplaceASIN.ASIN, user_id)
                // productInfo.price = price;
                productInfo.asin = item.Identifiers.MarketplaceASIN.ASIN; 
                productInfo.image = item.AttributeSets.ItemAttributes.SmallImage.URL
                productInfo.title = item.AttributeSets.ItemAttributes.Title;
                
                productArray.push(productInfo)
                productInfo = {};
                    console.log(productArray);
                }
            return productArray;
        }
        // console.log(productArray)
        return productArray
     } catch (e) {
        console.log("listMatchingProducts() ERROR:", e)
         throw new Error("Unabel to list matching products", e);
     }
};

const getAsinDetails = async(asin, user_id) => {
    try {
        let credentials = await getCredentials(user_id);
        let response = await amazonMws.products.search({
            'Version': '2011-10-01',
            'Action': 'GetMatchingProduct',
            'SellerId': credentials.sellerID,
            'MWSAuthToken': credentials.authToken,
            'MarketplaceId': credentials.marketplaceID,
            'ItemCondition': 'New',
            'ASINList.ASIN.1': asin
        });
        // console.log('response', JSON.stringify(response, null, 4));
        let data = {asin: asin};
        if (!response.Product) {
            data.price = 'Price not available';
            data.image = 'Image not available';
            data.title = 'Title not available';
        } else {
            if (response.Product.AttributeSets.ItemAttributes.ListPrice) {
                data.price = response.Product.AttributeSets.ItemAttributes.ListPrice.Amount;
            } else {
                data.price = 'Price not available';
            }
            if (response.Product.AttributeSets.ItemAttributes.SmallImage.URL) {
                data.image = response.Product.AttributeSets.ItemAttributes.SmallImage.URL;
            } else {
                data.image = 'Image not available';
            }
            if (response.Product.AttributeSets.ItemAttributes.Title) {
                data.title = response.Product.AttributeSets.ItemAttributes.Title;
            } else {
                data.title = null;
            }
        };
        return data

    } catch (e) {
        console.log("getAsinDetails ERROR:", e)
        throw new Error("Unable to get asin details", e);
    }
}

const getMyAsins = async (user_id) => {
    try {
        let credentials = await getCredentials(user_id);
        const response = await amazonMws.fulfillmentInventory.search({
            'Version': '2010-10-01',
            'Action': 'ListInventorySupply',
            'SellerId': credentials.sellerID,
            'MWSAuthToken': credentials.authToken,
            'MarketplaceId': credentials.marketplaceID,
            'QueryStartDateTime': new Date(12, 5, 2018)
        });
            // console.log(JSON.stringify(response, null, 2 ))
        let asins = response.InventorySupplyList.member.map((asin) => {
            return asin.ASIN;
        });
        return asins;
    } catch (e) {
        console.log("getMyAsins() ERROR:", e)
        throw new Error('Cannot get Products', e);
    }
};
//  listMatchingProducts("B0711LYY6V").then((response) => {
// console.log(response);
//  }).catch(e => console.log(e));

//  let foundAsin;
// getAsinFromAmazon('B06VX3NZY4').then(p => {
//     foundAsin = getPrices(p, 50);
// }).catch(e => console.log(e));

// setTimeout(() => {
//     console.log(foundAsin);
//     // getPrices(foundAsin);
// }, 2000)

// 'B0711LYY6V'
// 'B01GW5NXQY'
// 'B06VX3NZY4'

module.exports.getPrices = getPrices;
module.exports.getAsinFromAmazon = getAsinFromAmazon;
module.exports.listMatchingProducts = listMatchingProducts;
module.exports.getMyAsins = getMyAsins;
module.exports.getAsinDetails = getAsinDetails;
module.exports.getCredentials = getCredentials;