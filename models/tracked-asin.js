var mongoose = require('mongoose');

var trackedAsin = new mongoose.Schema({
    asin: {
        type: String,
        required: true
    },
    image: {
        type: String,
    },
    price: {
        type: String,
    },
    title: {
        type: String,
    },
    // hours: {
    //     type: String,
    // },
    minPrice: {
        type: String,
    },
    notified: {
        type: Boolean,
        default: false
    },
    _creator: {
        type: mongoose.Schema.Types.ObjectId,
        required: false,
    }
});

var TrackedAsin = mongoose.model('TrackedAsin', trackedAsin);
module.exports = TrackedAsin;