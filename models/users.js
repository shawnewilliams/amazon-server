var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var passportLocalMongoose = require('passport-local-mongoose');

var User = new Schema({

    // username and password automatically added by passport-local-mongoose
    // username: {
    //     type: String,
    //     required: true,
    //     unique: true
    // },
    // password: {
    //     type: String,
    //     required: true
    // },
    firstName: {
        type: String,
        default: ''
    },
    lastName: {
        type: String,
        default: ''
    },
    email: {
        type: String,
    },
    phoneNumber: {
        type: String,
    },
    credentials: {
        type: String,
        // unique: true
    }
});

User.plugin(passportLocalMongoose);

module.exports = mongoose.model('User', User);